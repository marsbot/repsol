require "lib/scss_lint"

activate :autoprefixer do |prefix|
  prefix.browsers = "> 5%"
end

activate :external_pipeline,
  name: :webpack,
  command: build? ?
  "BUILD_PRODUCTION=1 ./node_modules/webpack/bin/webpack.js -p --bail" :
  "BUILD_DEVELOPMENT=1 ./node_modules/webpack/bin/webpack.js -d --watch --color",
  source: ".tmp/dist",
  latency: 1

# Per-page layout changes
page '/*.xml', layout: false
page '/*.json', layout: false
page '/*.txt', layout: false

activate :directory_indexes

activate :blog do |blog|
  blog.layout = "article_layout"
  blog.sources = "blog/:year-:month-:day-:title.html"
  blog.permalink = "blog/:year-:month-:day-:title.html"
end

configure :development do
  activate :livereload do |livereload|
    livereload.livereload_css_target = "stylesheets/style.css"
    livereload.livereload_css_pattern = Regexp.new('_.*\.s?css')
  end
  activate :scss_lint
end

configure :build do
  set :build_dir, "public"

  activate :favicon_maker, :icons => {
    "_favicon_template.svg" => [
      { icon: "apple-touch-icon-180x180-precomposed.png" },
      { icon: "apple-touch-icon-152x152-precomposed.png" },
      { icon: "apple-touch-icon-144x144-precomposed.png" },
      { icon: "apple-touch-icon-120x120-precomposed.png" },
      { icon: "apple-touch-icon-114x114-precomposed.png" },
      { icon: "apple-touch-icon-76x76-precomposed.png" },
      { icon: "apple-touch-icon-72x72-precomposed.png" },
      { icon: "apple-touch-icon-60x60-precomposed.png" },
      { icon: "apple-touch-icon-57x57-precomposed.png" },
      { icon: "apple-touch-icon-precomposed.png", size: "57x57" },
      { icon: "apple-touch-icon.png", size: "57x57" },
      { icon: "favicon-196x196.png" },
      { icon: "favicon-160x160.png" },
      { icon: "favicon-96x96.png" },
      { icon: "favicon-32x32.png" },
      { icon: "favicon-16x16.png" },
      { icon: "favicon.png", size: "16x16" },
      { icon: "favicon.ico", size: "64x64,32x32,24x24,16x16" },
      { icon: "mstile-70x70.png", size: "70x70" },
      { icon: "mstile-144x144.png", size: "144x144" },
      { icon: "mstile-150x150.png", size: "150x150" },
      { icon: "mstile-310x310.png", size: "310x310" },
      { icon: "mstile-310x150.png", size: "310x150" }
    ]
  }

  activate :minify_css
  activate :minify_html
  activate :gzip
  activate :asset_hash
end
