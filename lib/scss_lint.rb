module Middleman
  module ScssLint
    class Extension < ::Middleman::Extension

      def run_once
        run_linter([])
      end

    end
  end
end
