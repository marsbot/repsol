---
title: ¿Comó funciona un turbocompresor?
cover_image: /blog/2017-05-18-como-funciona-un-turbocompresor/motor-turbo.jpg
---

Un turbo no es un concepto nada nuevo en el mundo de la automoción. Se trata de
un elemento que se inventó en los primeros años del siglo pasado por Alfred
J Büchi, un ingeniero de automoción. Pero no fue hasta la década de los 40,
cuando se empieza a implantar en instalaciones industriales, marinas
y ferroviarias. Ésta tecnología ha ido evolucionando a un ritmo, relativamente
lento, hasta que se ha ido incorporando en los vehículos equipados con motor
diésel, donde se ha hecho muy popular.

El porqué se ha popularizado tanto en los motores diésel, radica en la idea de
funcionamiento del propio motor. Si os acordáis del artículo sobre inyección
diésel, os comentábamos que, la forma en que se genera la combustión en este
tipo de motores es por compresión, por lo que no hay mezcla alguna de
aire-combustible en la carrera de compresión; sólo hasta que el pistón no llega
al punto muerto superior (donde termina la carrera de compresión) no se inyecta
el combustible.

Basándonos en esta idea de funcionamiento de exceso de aire, el turbo hace que
entre más cantidad de aire en el cilindro a base de comprimirlo, en condiciones
de misma cilindrada unitaria y régimen de motor (rpm). Además, las presiones que
se alcanzan al final de la carrera de compresión y sobre todo en la de expansión
son muy superiores (40-55 bar) que en los motores gasolina (15-25 bar).
