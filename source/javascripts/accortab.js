import $ from 'zepto-modules'

var accortab = {
  init: function (element) {
    $(element).each(function () {
      $('.js-content', this).addClass('is-hidden')
      $('.js-head', this).on('click', function () {
        $(this).toggleClass('is-active')
        $(this).next('.js-content').toggleClass('is-hidden')
        $(this)
          .siblings()
          .filter('.js-head')
          .each(function () {
            $(this)
              .next('.js-content')
              .addClass('is-hidden')
            $(this)
              .removeClass('is-active')
          })
      })
    })
  }
}

export default accortab
