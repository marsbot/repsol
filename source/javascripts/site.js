import initMap from './map.js'
import accordtab from './accortab.js'

if (document.getElementById('map')) {
  window.initMap = initMap
}

accordtab.init('.services')

var menuButton = document.getElementById('menu-button')
var menuNav = document.getElementById('menu')

menuButton.addEventListener('click', function () {
  menuButton.classList.toggle('open')
  menuNav.classList.toggle('navbar__menu--open')
})
