'use strict'

var webpack = require('webpack')
var path = require('path')

var DEVELOPMENT = Boolean(process.env.BUILD_DEVELOPMENT)
var PRODUCTION = Boolean(process.env.BUILD_PRODUCTION)

var entry = {
  site: './source/javascripts/site.js'
}

var resolve = {
  modules: [
    path.join(__dirname, '/source/javascripts'),
    'node_modules'
  ]
}

var resolve = {
  modules: [
    path.join(__dirname, '/source/javascripts'),
    'node_modules'
  ]
}

var modules = {
  rules: [
    {
      test: '/sw.js$/',
      loader: 'ignore-loader'
    },
    {
      test: /source\/javascripts\/.*\.js$/,
      exclude: /node_modules|\.tmp|vendor/,
      loader: 'babel-loader',
      options: {
        presets: [
          ['env', {
            "targets": {
              "browsers": ["last 1 versions", "> 5% in MX"]
            },
            'debug': DEVELOPMENT,
            'modules': false
          }],
        ],
        cacheDirectory: true
      }
    }
  ]
}

if( DEVELOPMENT ) {
  modules.rules.unshift({
    enforce: 'pre',
    test: /source\/javascripts\/.*.js$/,
    exclude: /node_modules|\.tmp|vendor/,
    loader: 'eslint-loader'
  })
}

var plugins = [
  new webpack.DefinePlugin({
    __DEVELOPMENT__: DEVELOPMENT,
    __PRODUCTION__: PRODUCTION
  })
]

var output = {
  path: path.join(__dirname, '/.tmp/dist'),
  filename: 'javascripts/[name].js',
  sourceMapFilename: 'javascripts/[name].map'
}

module.exports = {
  devtool: DEVELOPMENT ? 'cheap-source-map' : 'hidden-source-map',
  target: 'web',
  entry: entry,
  resolve: resolve,
  module: modules,
  plugins: plugins,
  output: output
}
